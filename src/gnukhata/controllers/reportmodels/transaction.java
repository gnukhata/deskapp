/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/


package gnukhata.controllers.reportmodels;

public class transaction {
	private String transactionDate;
	private String particulars;
	private String voucherNo;
	private String dr;
	private String cr;
	private String narration;
	private String voucherCode;
	public transaction(String transactionDate, String particulars,
			String voucherNo, String dr, String cr, String narration, String voucherCode) {
		super();
		this.transactionDate = transactionDate;
		this.particulars = particulars;
		this.voucherNo = voucherNo;
		this.dr = dr;
		this.cr = cr;
		this.narration = narration;
		this.voucherCode = voucherCode;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public String getParticulars() {
		return particulars;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public String getDr() {
		return dr;
	}
	public String getCr() {
		return cr;
	}
	public String getNarration() {
		return narration;
	}
	
	/**
	 * @return the voucherCode
	 */
	public int getVoucherCode() {
		return Integer.parseInt(this.voucherCode);
	}
	@Override
	public String toString() {
		return "transaction [voucherNo=" + voucherNo + "]";
	}


}
