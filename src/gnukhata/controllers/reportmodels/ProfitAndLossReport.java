/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/


package gnukhata.controllers.reportmodels;

public class ProfitAndLossReport {
	private String to;
	private String accountName;
	private String amount;
	private String by;
	private String accountName1;
	private String amount1;
	public ProfitAndLossReport(String to, String accountName, String amount, String by,
			String accountName1, String amount1){
		super();
		this.to=to;
		this.accountName=accountName;
		this.amount=amount;
		this.by=by;
		this.accountName1=accountName1;
		this.amount1=amount1;
	}
	

	public String getto() {
		return to;
	}
	
	public String getaccountname() {
		return accountName;
	}
	
	public String getamount() {
		return amount;
	}
	
	public String getby() {
		return by;
	}
	
	public String getaccountname1() {
		return accountName1;
	}
	
	public String getamount1() {
		return amount1;
	}
	
	

}


