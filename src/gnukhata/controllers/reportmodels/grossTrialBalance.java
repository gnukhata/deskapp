/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/


package gnukhata.controllers.reportmodels;

public class grossTrialBalance {
	
	private String srNo;
	private String accountName;
	private String groupName;
	private String totaldr;
	private String totalcr;
	
	public grossTrialBalance(String srNo, String accountName, String groupName,
			String totaldr, String totalcr) {
		// TODO Auto-generated constructor stub
		
		super();
		this.srNo=srNo;
		this.accountName=accountName;
		this.groupName=groupName;
		this.totaldr=totaldr;
		this.totalcr=totalcr;
	}

	/**
	 * @return the srNo
	 */
	public String getSrNo() {
		return srNo;
	}

	/**
	 * @return the accountName
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @return the totaldr
	 */
	public String getTotaldr() {
		return totaldr;
	}

	/**
	 * @return the totalcr
	 */
	public String getTotalcr() {
		return totalcr;
	}

	
	@Override
	public String toString() {
		return this.accountName;
	}
	
	

}
