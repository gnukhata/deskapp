/* This file is part of GNUKhata:A modular,robust and Free Accounting System.

  GNUKhata is Free Software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of
  the License, or (at your option) any later version.and old.stockflag = 's'

  GNUKhata is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public
  License along with GNUKhata (COPYING); if not, write to the
  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA  02110-1301  USA59 Temple Place, Suite 330*/

package gnukhata.views;
import gnukhata.globals;
import gnukhata.controllers.accountController;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ArrayList;

import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
 
public class Get_Preferences extends Dialog {

 
  /**
   * @param parent
   */
  public Get_Preferences(Shell parent) {
    super(parent);
  }
 
  /**
   * @param parent
   * @param style
   */
  public Get_Preferences(Shell parent, int style) {
    super(parent, style);
  }
 
  
  Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	
String value;


	




/**
 * Makes the dialog visible.
 *
 * @return
 */

public String open() {
  Shell parent = getParent();
  final Shell shell = new Shell(parent, SWT.TITLE | SWT.BORDER | SWT.APPLICATION_MODAL);
  shell.setText("Connect to Server");

  shell.setLayout(new GridLayout(2, true));
  

  Label lblSvlist = new Label(shell, SWT.NULL);
  lblSvlist.setText("&Server List:");
  
  final CCombo dropdownSv = new CCombo(shell, SWT.DROP_DOWN|SWT.READ_ONLY);
  GridData data = new GridData();
  data.widthHint = 250;
  dropdownSv.setLayoutData(data);
  
  Label lblcheck = new Label(shell, SWT.NULL);
  lblcheck.setText("Add new");
 
  final Button btncheck = new Button(shell, SWT.CHECK);
  btncheck.setLayoutData(new GridData(GridData.CENTER));
  
 
  final Label lblSvname = new Label(shell, SWT.NULL);
  lblSvname.setText("Ser&ver Name:");
  lblSvname.setVisible(false);
  
  final Text txtSvname = new Text(shell, SWT.DOUBLE_BUFFERED| SWT.BORDER);
  data.widthHint = 250;
  txtSvname.setLayoutData(data);
  txtSvname.setVisible(false);
  
  
  final Label lblAddr = new Label(shell, SWT.NULL);
  lblAddr.setText("&Address:");
  lblAddr.setVisible(false);
  
  final Text txtAddr = new Text(shell, SWT.DOUBLE_BUFFERED|SWT.BORDER);
  data.widthHint = 250;
  txtAddr.setLayoutData(data);
  txtAddr.setVisible(false);
  
  
  
  final Button btnConnect = new Button(shell, SWT.PUSH);
  btnConnect.setText("Co&nnect");
  btnConnect.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_END));
  
  final Button buttonCancel = new Button(shell, SWT.PUSH);
  buttonCancel.setText("C&ancel");
  ArrayList<String> Slist = new ArrayList<String>();
  dropdownSv.removeAll();
  dropdownSv.add("Localhost -127.0.0.1",0);
  try
  {
     FileInputStream fileIn = new FileInputStream("src/Reports/ServerList");
     ObjectInputStream in = new ObjectInputStream(fileIn);
     Slist = (ArrayList<String>) in.readObject();
     for (int i = 0; i < Slist.size(); i++ )
 	{
 		dropdownSv.add(Slist.get(i));
 	}
     in.close();
     fileIn.close();
  }catch(Exception i)
  {
     i.printStackTrace();
    
  }
  
	
	
	dropdownSv.setFocus();
	dropdownSv.select(0);


  
  Background =  new Color(this.getDisplay() ,220 , 224, 227);
  Foreground = new Color(this.getDisplay() ,0, 0,0 );
  FocusBackground  = new Color(this.getDisplay(),78,97,114 );
  FocusForeground = new Color(this.getDisplay(),255,255,255);
  BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);

  
  btncheck.addKeyListener(new KeyAdapter() {
	  
	  @Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.keyCode==SWT.ARROW_DOWN)
		{
			txtSvname.setFocus();
		}
		
		if(arg0.keyCode==SWT.ARROW_UP)
		{
			dropdownSv.setFocus();
		}
	}
});
  
  btncheck.addSelectionListener(new SelectionAdapter() {
	  @Override
	public void widgetSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub
		if (dropdownSv.isVisible()) {
			dropdownSv.setVisible(false);
			lblSvname.setVisible(true);
			txtSvname.setVisible(true);
			lblAddr.setVisible(true);
			txtAddr.setVisible(true);
		}
		else
		{
			dropdownSv.setVisible(true);
			lblSvname.setVisible(false);
			txtSvname.setVisible(false);
			lblAddr.setVisible(false);
			txtAddr.setVisible(false);
		}
	}
});
  
  dropdownSv.addKeyListener(new KeyAdapter() {
	  @Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.keyCode==SWT.CR || arg0.keyCode == SWT.KEYPAD_CR) {
			btnConnect.notifyListeners(SWT.Selection, new Event());
		}
	}
});
  
  
   btnConnect.addFocusListener(new FocusAdapter() {
  	@Override
  	public void focusGained(FocusEvent arg0) {
  		btnConnect.setBackground(FocusBackground);
  		btnConnect.setForeground(BtnFocusForeground);
  	
  		//super.focusGained(arg0);
  	}
  	@Override
  	public void focusLost(FocusEvent arg0) {
  		btnConnect.setBackground(Background);
  		btnConnect.setForeground(Foreground);
  	//	super.focusLost(arg0);
  	}
	});
  
   buttonCancel.addFocusListener(new FocusAdapter() {
   	@Override
   	public void focusGained(FocusEvent arg0) {
   		buttonCancel.setBackground(FocusBackground);
   		buttonCancel.setForeground(BtnFocusForeground);
   	
   		
   	}
   	@Override
   	public void focusLost(FocusEvent arg0) {
   		buttonCancel.setBackground(Background);
   		buttonCancel.setForeground(Foreground);
   		// TODO Auto-generated method stub
   	//	super.focusLost(arg0);
   	}
	});
   
   txtSvname.addKeyListener(new KeyAdapter() {
	   public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.keyCode==SWT.ARROW_DOWN)
			{
				txtAddr.setFocus();
			}
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				btncheck.setFocus();
			}
			if(arg0.keyCode==SWT.ESC)
			{
				buttonCancel.notifyListeners(SWT.Selection, new Event());
			}
		}
});
   
   txtAddr.addKeyListener(new KeyListener() {
		
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
		
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtSvname.setFocus();
			}
			
			if(arg0.keyCode==SWT.CR||arg0.keyCode==SWT.KEYPAD_CR)
			{
				
				btnConnect.notifyListeners(SWT.Selection, new Event());
			}
			if(arg0.keyCode==SWT.ESC)
			{
				buttonCancel.notifyListeners(SWT.Selection, new Event());
			}
		}
	});
   
   btnConnect.addKeyListener(new KeyAdapter()
   {
	   @Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		//super.keyPressed(arg0);
		   if(arg0.keyCode==SWT.ARROW_RIGHT)
			{
				buttonCancel.setFocus();
			}
	}
   });
   
   buttonCancel.addKeyListener(new KeyAdapter()
   {
	   @Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		//super.keyPressed(arg0);
		   if(arg0.keyCode==SWT.ARROW_LEFT)
			{
				btnConnect.setFocus();
			}
	}
   });
   
   txtAddr.addVerifyListener(new VerifyListener() {
		
		@Override
		public void verifyText(VerifyEvent arg0) {
			// TODO Auto-generated method stub
			switch (arg0.keyCode) {
           case SWT.BS:           // Backspace
           case SWT.DEL:          // Delete
           case SWT.HOME:         // Home
           case SWT.END:          // End
           case SWT.ARROW_LEFT:   // Left arrow
           case SWT.ARROW_RIGHT:  // Right arrow
           case SWT.TAB:
           case SWT.CR:
           case SWT.KEYPAD_CR:
           case SWT.KEYPAD_DECIMAL:
               return;
			}
						
	if(arg0.keyCode == 46)
	{
		return;
	}
	if(arg0.keyCode == 45||arg0.keyCode == 62)
	{
		  arg0.doit = false;
		
	}
       if (!Character.isDigit(arg0.character)) {
           arg0.doit = false;  // disallow the action
       }
       

			
		}
	});
  

	btnConnect.addListener(SWT.Selection, new Listener() {
	      public void handleEvent(Event event) {
	    	  try {
	    	  if (txtAddr.isVisible()&txtAddr.getText().equals(""))
				{
	    		  MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
					errMessage.setText("Error!");
					errMessage.setMessage("Please enter the URL");
					errMessage.open();	
					txtAddr.setFocus();
					return;
				}
	    	
	              if (!dropdownSv.isVisible()) {
	    			value = new String("http://" + txtAddr.getText() + ":7081");
	    		}
	              else {
					String[] ip = dropdownSv.getItem(dropdownSv.getSelectionIndex()).toString().split("-");
					for (int i = 0; i < ip.length; i++) {
						System.out.println("bbbbbbbbbbbbbb"+ip[i]);
					}
					value = new String("http://" + ip[1] + ":7081");
					System.out.println("aaaaaaaaaaaaaaaaa  " + value);
				}
	    		
	          
	    	  
	    		  try
	    		  {
	    			  XmlRpcClientConfigImpl	  conf = new XmlRpcClientConfigImpl();
					
					conf.setServerURL(new URL(value));
					globals.client.setConfig(conf);
					Object[] params = new Object[] {};
					Object[] result = (Object[]) gnukhata.globals.client.execute("getOrganisationNames", params);
					
					System.out.println("result is" + result);
					String[] names = new String[result.length +1];
					names[0] = "--select--";
					// this.finyear[]=new Strng ();
					for (int i = 0; i < result.length; i++)
					{
						System.out.println("result[ " + i + "] is " + result[i]);
						names[i +1] = result[i].toString();
					}
				if(!dropdownSv.isVisible())
				{
					try
				      {
						ArrayList<String> svlist = new ArrayList<String>();
						for (int i = 1; i < dropdownSv.getItemCount(); i++) {
							svlist.add(dropdownSv.getItem(i));
						}
						svlist.add(txtSvname.getText().trim()+" -"+txtAddr.getText().trim());
						
				         FileOutputStream fileOut =
				         new FileOutputStream("src/Reports/ServerList");
				         ObjectOutputStream out = new ObjectOutputStream(fileOut);
				         out.writeObject(svlist);
				         out.close();
				         fileOut.close();
				         System.out.printf("Serialized");
				      }catch(IOException i)
				      {
				          i.printStackTrace();
				      }
	    		  }
	    		  }
	    		  catch(Exception e)
	    		  {
	    			e.printStackTrace();
	    			
	    			MessageBox errMessage = new MessageBox(new Shell(),SWT.OK| SWT.ERROR );
	  				errMessage.setText("Error!");
	  				errMessage.setMessage("Incorrect URL");
	  				errMessage.open();	
	  				txtAddr.setFocus();
	  				return;
	    		  }
	    		  shell.dispose();
			        
					
				
	    	  
	    	  }catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
	      }
	      
	    });
	

	buttonCancel.addKeyListener(new KeyAdapter() {
		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyReleased(arg0);
			if(arg0.keyCode==SWT.ARROW_UP)
			{
				txtAddr.setFocus();
			}			
			else
			{						
				arg0.doit = false;
			}
			if(arg0.keyCode==SWT.ARROW_LEFT)
			{
				btnConnect.setFocus();
			}
		}
	});
/*
	buttonCancel.addKeyListener(new KeyAdapter() {
		public void keyPressed(KeyEvent e){
			
			if(e.keyCode == SWT.ARROW_LEFT);
			{
				btnSave.setFocus();
			}
		}
		
	});*/
	
	dropdownSv.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0)
		{
			dropdownSv.setBackground(Background);
			dropdownSv.setForeground(Foreground);
			dropdownSv.setListVisible(false);
		}
		@Override
		public void focusGained(FocusEvent arg0) {
			//dropdownGroupName.setListVisible(true);
			dropdownSv.setBackground(FocusBackground);
			dropdownSv.setForeground(FocusForeground);
			dropdownSv.setListVisible(true);
			// TODO Auto-generated method stub
		//	super.focusGained(arg0);
		}
	});
	
	
	txtSvname.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			txtSvname.setBackground(Background);
			txtSvname.setForeground(Foreground);
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			
				
						

		}
		@Override
		public void focusGained(FocusEvent arg0) {
			txtSvname.setBackground(FocusBackground);
			txtSvname.setForeground(FocusForeground);
			// TODO Auto-generated method stub
		//	super.focusGained(arg0);
		}
	});
	
	txtAddr.addFocusListener(new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent arg0) {
			txtAddr.setBackground(Background);
			txtAddr.setForeground(Foreground);
			// TODO Auto-generated method stub
			//super.focusLost(arg0);
			
				
						

		}
		@Override
		public void focusGained(FocusEvent arg0) {
			txtAddr.setBackground(FocusBackground);
			txtAddr.setForeground(FocusForeground);
			// TODO Auto-generated method stub
		//	super.focusGained(arg0);
		}
	});




  buttonCancel.addListener(SWT.Selection, new Listener() {
    public void handleEvent(Event event) {
      value = null;
      
      shell.dispose();
    }
  });
  
  shell.addListener(SWT.Traverse, new Listener() {
      public void handleEvent(Event event) {
        if(event.detail == SWT.TRAVERSE_ESCAPE)
          event.doit = false;
      }
    });
 // text.setText("");
  shell.pack();
  shell.open();
  

  Display display = parent.getDisplay();
  while (!shell.isDisposed()) {
    if (!display.readAndDispatch())
      display.sleep();
  }

  return value;
}

private Device getDisplay() {
	// TODO Auto-generated method stub
	return null;
}



public static void main(String[] args) {

}
  
 
}